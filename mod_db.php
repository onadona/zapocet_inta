<?php 

if (!defined("TIMEOUT")) define ("TIMEOUT", 1200);//trvání přihlášení v sekundách
if (!defined("SQL_HOST")) define('SQL_HOST', 'localhost'); 
if (!defined("SQL_DBNAME")) define('SQL_DBNAME', 'events');
if (!defined("SQL_USERNAME")) define('SQL_USERNAME', 'root');
if (!defined("SQL_PASSWORD")) define('SQL_PASSWORD', 'admin');

function getPDO(){
	if(empty($pdo)){		
		$dsn = 'mysql:dbname=' . SQL_DBNAME . ';host=' . SQL_HOST . '';
		$user = SQL_USERNAME;
		$password = SQL_PASSWORD;
		
		try {
			$pdo = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
					
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			 
			$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			
		} catch (PDOException $e) {
			die('Connection failed: ' . $e->getMessage());
		}
	}
	return $pdo;
}

 function getPredmety($count, $page) {
	
	$pdo = getPDO();
	
	if($page > 0)$page--;
	else if($page < 0) $page=0;
	$starts = intval($count) * intval($page);
	$dotaz = $pdo->prepare("SELECT * FROM event ORDER BY eventId DESC LIMIT ".$count." OFFSET ".$starts);  
	$dotaz->execute();
	
	$posts = $dotaz->fetchAll();
	
	return $posts; 
}
function insertPredmet($school, $things){
	$pdo = getPDO();
	$dotaz = $pdo->prepare("INSERT INTO event (school, job) VALUES (? ,?)");
	$dotaz->execute(array($school, $things));
}
?>